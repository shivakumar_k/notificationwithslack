import requests
import os
import csv
from config import channel_id,slack_auth_token,slack_api_token, pipeline_base_url,pipeline_auth, cfra_base_url, cfra_auth
from slack_module import SlackAPI
from datetime import date,timedelta

root = os.path.dirname(os.path.realpath(__file__))

ca_etfuniverse_file =  os.path.join(root,"etf_universe_canada.csv")

exception_dir = os.path.join(root,'exceptions')
if not os.path.exists(exception_dir):
    os.mkdir(exception_dir)
    print ("{} created".format(exception_dir))
else:
    print ("exceptions files will be creating here: {}".format(exception_dir))



# read csv file
existing_etf_list = {}
new_etf_list = []
mismatched_etf_list = []

with open(ca_etfuniverse_file, 'r') as existing_file:
    efile_reader = csv.DictReader(existing_file)
    for line in efile_reader:
        try:
            this_row = {}
            this_row["firstbridge_id"] = line["firstbridge_id"]
            this_row["bloomberg_symbol"] = line["bloomberg_symbol"]
            this_row["is_live_listed"] = line["is_live_listed"]
            existing_etf_list[line["firstbridge_id"]] = line["is_live_listed"]
        except:
            pass

etf_response = requests.get(cfra_base_url+"/api/v1.0/etf_list_by_filter?listing_country_code=CA ", headers=cfra_auth)
onlyResult = etf_response.json()
latest_etf_data = onlyResult['results']

for line in latest_etf_data:
    try:
        this_row = {}
        this_row["firstbridge_id"] = line["firstbridge_id"]
        this_row["bloomberg_symbol"] = line["bloomberg_symbol"]
        this_row["is_live_listed"] = line["is_live_listed"]
        new_etf_list.append(this_row)
        if existing_etf_list[this_row["firstbridge_id"]] == 'True' and this_row["is_live_listed"] == False:
            mismatched_etf_list.append(this_row)
    except:
        pass


new_csv_with_data = open(ca_etfuniverse_file, 'w+', newline='')
keys = new_etf_list[0].keys()
dict_writer = csv.DictWriter(new_csv_with_data, keys)
dict_writer.writeheader()
dict_writer.writerows(new_etf_list)
new_csv_with_data.close()


class CreateCSV:
    def __init__(self,filepath,data):
        self.filepath = filepath
        self.data = data

    def create_file(self):
        try:
            csv_columns = ['firstbridge_id','bloomberg_symbol','is_live_listed']
            with open(self.filepath,'w', newline='') as fi:
                writer = csv.DictWriter(fi, fieldnames=csv_columns)
                writer.writeheader()
                for data in self.data:
                    writer.writerow(data)
            return ("{} created".format(self.filepath))
        except Exception as e:
            return ("err in creating {}:{}".format(self.filepath,e))

class Notifications:

    def __init__(self, base_url, auth_token, notification_head, notification_body, to_user):
        self.head = notification_head
        self.body = notification_body
        self.user = to_user
        self.auth_token = auth_token
        self.base_url = base_url

    def send2cms(self):
        payload = {
            "head": self.head,
            "body": self.body,
            "to_user": self.user
        }
        resp_raw = requests.post(self.base_url + "/api/v1.0/notification/create", json=payload, headers=self.auth_token,
                                 timeout=3)
        resp = resp_raw.json()
        return resp


# #Indian Standard Time is 9 hours and 30 minutes ahead of Eastern Time so when cron set pass t-1 date to fetch current slack fileurl
start_date = end_date = date.today() - timedelta(days=1)
slack_obj = SlackAPI(slack_auth_token,slack_api_token,channel_id,start_date,end_date)

filepath = os.path.join(exception_dir,'canada_closure_notification.csv') 
print (CreateCSV(filepath,mismatched_etf_list).create_file())    
print (slack_obj.upload_file(filepath,channel_name='#classification_alerts'))


di = slack_obj.get_file_url()

user_id           = 'ab95e7c8-6c69-467a-9a21-a8a3d848146d' # Mohit user id for testing
for filename,slack_url in di.items():
    if filename == 'canada_closure_notification.csv':
        notification_head = (filename.split('.')[0].replace('_',' ')+' check')
        alert_detail = "<p><a href='{}' target='_blank'>download {}</a></p>".format(slack_url,filename)
        notification_body = "<p>{}</p>".format(alert_detail)
        obj = Notifications(pipeline_base_url,pipeline_auth,notification_head,notification_body,user_id)
        print (obj.send2cms())
